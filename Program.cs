﻿using System;

namespace ex_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello I would like you to enter your month of birth");

            var mOB = (Console.ReadLine());
            Console.WriteLine($"your Month of birth is {mOB}");
            
            //End the program with line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}